#pragma once
#include <filesystem>    // for std::filesystem::path
#include <stdexcept>     // for std::logic_error
#include <cstddef>       // for std::size_t
#include <string>        // for std::string
#include <string_view>   // for std::string_view
#include <unordered_map> // for std::unordered_map
#include <array>         // for std::array
#include <tuple>         // for std::tuple_element_t
#include <algorithm>     // for std::find, std::find_if
#include <fstream>       // for std::ifstream
#include <utility>       // for std::forward, std::index_sequence
#include <cctype>        // for std::isalnum, std::isspace
#include <optional>      // for std::optional
#include "lzhlib/reader/dynamic_reader.hpp" // for lzhlib::reader::dynamic_reader
namespace lzhlib
{
    template <typename TypeList>
    class config_file_reader
    {
        template <std::size_t Index>
        using elem_t = std::tuple_element_t<Index, typename TypeList::types>;
        using reader_t = lzhlib::reader::dynamic_reader<TypeList>;
    public:
        constexpr static std::size_t size = TypeList::size;
        template <std::size_t Index>
        using reader_prototype = elem_t<Index>(std::string_view);

        template <typename ...Args>
        explicit config_file_reader(std::array<std::string_view, size> const&names, Args &&...args)
            :  reader(std::forward<Args>(args)...)
        {
            for (std::size_t i = 0; i < size; ++i)
            {
                name_map[std::string(names[i])] = i;
            }
        }
        template <typename ...Args>
        explicit config_file_reader(std::array<std::string_view, size> const&names, std::string_view path, Args &&...args)
            :  reader(std::forward<Args>(args)...)
        {
            for (std::size_t i = 0; i < size; ++i)
            {
                name_map[std::string(names[i])] = i;
            }
            read(path);
        }
        void read(std::string_view path)
        {
            std::filesystem::path file_name = path;
            std::ifstream file(file_name);
            read_file(file);
        }
        template <std::size_t Index>
        decltype(auto) get()
        {
            return reader.template get<Index>().value();
        }
        template <std::size_t Index>
        decltype(auto) get() const
        {
            return reader.template get<Index>().value();
        }
        template <std::size_t Index>
        decltype(auto) cget() const
        {
            return reader.template cget<Index>().value();
        }

    private:

        std::unordered_map<std::string, std::size_t> name_map;
        reader_t reader;

        void read_file(std::ifstream &file)
        {
            using namespace std::literals;
            std::string line;
            std::size_t line_no = 0;
            while (std::getline(file, line))
            {
                ++line_no;
                try
                {
                    read_line(line, line_no);
                }
                catch (std::logic_error const &e)
                {
                    throw std::logic_error(std::string(e.what()) + " at line of " + std::to_string(line_no) + ".");
                }
            }
            if (!is_all_set(std::make_index_sequence<size>{}))
                throw std::logic_error("not all variable set in config file.");
        }
        void read_line(std::string_view line, std::size_t line_no)
        {
            auto id_begin_opt = begin_of_id(line);
            if (!id_begin_opt)
                return;
            auto id_begin = *id_begin_opt;
            auto id_end = std::find_if(id_begin + 1, line.end(), [](char c)
            {
                return !std::isalnum(c) && c != '_';
            });
            auto assign_pos = std::find(id_end, line.end(), '=');
            if (assign_pos == line.end())
                throw std::logic_error("expect '='");
            auto eat_space = std::find_if(assign_pos + 1, line.end(), [](char c)
            {
                return !std::isspace(c);
            });
            std::string_view remaining(&*eat_space, line.end() - eat_space);
            std::string variable_name(id_begin, id_end);
            auto iter = name_map.find(variable_name);
            if (iter == name_map.end())
                throw std::logic_error("invalid variable name");
            reader.read(iter->second, remaining);
        }
        static std::optional<std::string_view::iterator> begin_of_id(std::string_view str)
        {
            for (auto iter = str.begin(); iter != str.end(); ++iter)
                if (!isspace(*iter))
                {
                    if (*iter == '#')
                        return std::nullopt;
                    else if (std::isalpha(*iter) || *iter == '_')
                        return iter;
                    else
                        throw std::logic_error("expect identifier");
                }
            return std::nullopt;
        }
        template <std::size_t ...Indices>
        bool is_all_set(std::index_sequence<Indices...>)
        {
            return ((reader).template get<Indices>().has_value() && ...);
        }
    };
}