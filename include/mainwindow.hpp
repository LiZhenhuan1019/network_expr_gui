#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "include/logger.hpp"
#include "include/http_server.hpp"
#include "include/http_server_config_reader.hpp"
#include <lzhlib/thread/queue.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
signals:
    void change_text();
private slots:

    void on_start_button_clicked();

    void on_stop_button_clicked();

    void changing_text();
private:
    Ui::MainWindow *ui;
    lzhlib::string_logger string_logger;
    using logger_t = lzhlib::thread_safe_logger<lzhlib::string_logger>;
    lzhlib::thread_safe_logger<lzhlib::string_logger> logger;
    lzhlib::winsock2_api_handler handler;
    std::optional<lzhlib::http_server_config_reader> config;
    std::optional<lzhlib::http_server<logger_t>> server;
    lzhlib::thread::queue<std::string> logger_outputs;

    void start();
    void stop();
    void update_output(std::string_view s);
};

#endif // MAINWINDOW_H
