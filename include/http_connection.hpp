#ifndef NETWORK_EXPR_HTTP_CONNECTION_HPP
#define NETWORK_EXPR_HTTP_CONNECTION_HPP

#include <fstream>         // for std::ifstream
#include <functional>      // for std::functional
#include <regex>           // for std::regex, std::regex_match, std::cmatch, std::cregex_iterator
#include <filesystem>      // for std::filesystem::path, etc.
#include <sstream>         // for std::ostringstream
#include <string>          // for std::string
#include <string_view>     // for std::string_view
#include <unordered_map>   // for std::unordered_map
#include <cstddef>         // for std::size_t
#include <algorithm>       // for std::max
#include "socket.hpp"      // for lzhlib::tcp_communicate_socket
#include "http_server_config.hpp" // for lzhlib::http_server_config
#include "http_exception.hpp"
#include "content_type_table.hpp" // for lzhlib::get_content_type
namespace lzhlib
{
    using field_map = std::unordered_map<std::string_view, std::string_view>;
    struct http_request
    {
        std::string_view whole_message;
        std::string_view method;
        std::string_view url;
        std::string_view http_version;
        field_map header_fields;
        std::string_view body;
    };
    struct http_response
    {
        std::string_view http_version;
        std::string_view status_code;
        std::string_view reason_phase;
        field_map header_fields;
        std::string_view body;
    };
    template <typename LoggerT>
    struct http_connection
    {
        using logger_t = LoggerT;
        explicit http_connection(tcp_communicate_socket socket, http_server_config const &config, logger_t &logger)
            : socket(socket), config(config), logger(logger)
        {
            log_socket(logger);
        }
        int operator()()
        {
            try
            {
                if (int result = socket.receive(received))
                {
                    http_request request = process_message(received);
                    process_request(request);
                }
            }
            catch (http_exception const &e)
            {
                log_exception(logger, e);
                send_error_response(e.error_code);
            }
            return 0;
        }
        void service_unavailable()
        {
            send_error_response("503");
        }
    private:
        tcp_communicate_socket socket;
        http_server_config const &config;
        std::string received;
        std::string response_text;
        logger_t &logger;

        http_request process_message(std::string_view message)
        {
            std::size_t crlf = message.find("\n");
            if (crlf == std::string_view::npos)
                throw http_bad_request("expect \\n");
            std::string_view first_line = message.substr(0, crlf + 1), header, body;
            std::size_t end_of_head = message.find("\r\n\r\n");
            if (end_of_head == std::string_view::npos)
            {
                end_of_head = message.find("\n\n");
                if (end_of_head == std::string_view::npos)
                    throw http_bad_request("expect \\n\\n");
                header = message.substr(crlf + 1, end_of_head - crlf);
                body = message.substr(end_of_head + 2);
            }
            else
            {
                header = message.substr(crlf + 1, end_of_head + 1 - crlf);
                body = message.substr(end_of_head + 4);
            }
            std::regex first_line_regex(R"~(([\da-zA-Z!#$%&'*+-.^_`|~]+) ([^ ]+) HTTP\/(\d\.\d)\r?\n)~");
            std::regex header_regex(
                R"~((?:([\da-zA-Z!#$%&'*+-.^_`|~]+):[ \t]*((?:(?:[\u0021-\u007e\u0080-\u00ff](?:[ \t]+[\u0021-\u007e\u0080-\u00ff])?)|(?:\r?\n[ \t]+))*)[ \t]*\r?\n))~");
            std::cmatch match;
            bool success = std::regex_match(first_line.begin(), first_line.end(), match, first_line_regex);
            if (!success || match.size() != 4)
                throw http_bad_request("parse start line failed.");
            http_request request;
            request.whole_message = message;
            request.method = to_string_view(match[1]);
            request.url = to_string_view(match[2]);
            request.http_version = to_string_view(match[3]);
            for (auto iter = std::cregex_iterator(header.begin(), header.end(), header_regex); iter != std::cregex_iterator(); ++iter)
            {
                if (iter->size() != 3)
                    throw http_bad_request("parse header field failed");
                request.header_fields[to_string_view((*iter)[1])] = to_string_view((*iter)[2]);
            }
            request.body = body;
            return request;
        }
        void process_request(http_request const &request)
        {
            log_request(logger, request);
            auto method = method_map.find(request.method);
            using namespace std::literals;
            if (method == method_map.end())
                throw http_method_not_supported("method of \""s.append(request.method).append("\" is not supported"));
            method->second(*this, request);
        };

        void method_get(http_request const &request)
        {
            std::string content_type{lzhlib::content_type::get_content_type(std::filesystem::path(request.url).extension().string())};
            add_encoding_for_text_type(content_type);
            std::string resource_content = read_resource(request.url);
            send_resource(resource_content, std::move(content_type));
        }

        void add_encoding_for_text_type(std::string &content_type)
        {
            auto iter = content_type.find("text/");
            if (iter == 0)
            {
                content_type += "; charset=utf-8";
            }
        }

        void send_resource(std::string_view resource_content, std::string content_type)
        {
            http_response response =
                {
                    "1.1", "200", "OK",
                    {
                        {"Accept-Ranges", "none"},
                        {"Content-Length", std::to_string(resource_content.size())},
                        {"Content-Type", std::move(content_type)},
                        {"Connection", "close"}
                    },
                    resource_content
                };
            send_response(response);
        }
        std::string read_resource(std::string_view url)
        {
            namespace fs = std::filesystem;
            using namespace std::literals;
            fs::path resource_path = get_absolute_path(url);
            if (!fs::exists(resource_path))
                throw http_not_found("resource of \""s.append(url).append("\" is not found : file not existed."));
            if (std::ifstream file{resource_path, std::ios::binary | std::ios::ate})
            {
                auto size = file.tellg();
                std::string str(size, '\0');
                file.seekg(0);
                if (file.read(str.data(), size))
                {
                    return str;
                }
            }
            throw http_service_unavailable("resource of \""s.append(url).append("\" is unavaiable : file stream failed."));
        }
        void send_error_response(std::string_view error_code)
        {
            http_response response =
                {
                    "1.1", std::string(error_code), "",
                    {
                        {"Connection", "close"},
                        {"Content-Length", "0"}
                    },
                };
            send_response(response);
        }
        void send_response(http_response const &response)
        {
            log_response(logger, response);
            response_text.clear();
            response_text.reserve(std::max(response_text.capacity(), response.body.size() + 100));
            response_text.append("HTTP/").append(response.http_version).append(" ").append(response.status_code).append(" ").append(response.reason_phase).append("\r\n");
            for (auto &[name, value] : response.header_fields)
            {
                response_text.append(name).append(": ").append(value).append("\r\n");
            }
            response_text += "\r\n";
            response_text += response.body;
            socket.send(response_text);
        }
        std::filesystem::path get_absolute_path(std::string_view path)
        {
            namespace fs = std::filesystem;
            fs::path url = path;
            url = relative_of_lexically_restricted_path(url);
            fs::path document_root = config.document_root;
            return document_root / url;
        }
        static std::filesystem::path relative_of_lexically_restricted_path(std::filesystem::path const &path)
        {
            namespace fs = std::filesystem;
            if (path.is_absolute())
                return path.lexically_normal();
            else
                return (fs::current_path().root_path() / path).lexically_normal().relative_path();
        }
        static void log_response(logger_t &logger, http_response const &response)
        {
            std::ostringstream stream;
            stream << "new response \n{\n" << indent << "HTTP/" << response.http_version << " " << response.status_code << " " << response.reason_phase << "\n";
            stream << indent << "header fields: \n" << indent << "{\n";
            for (auto &[name, value] : response.header_fields)
            {
                stream << indent << indent << "\"" << name << "\" : \"" << value << "\"\n";
            }
            stream << indent << "}end header fields\n";
            stream << "}end response\n";
            auto lock = logger.lock();
            logger << stream.str() << logger.flush;
        }
        static void log_request(logger_t &logger, http_request const &request)
        {
            std::ostringstream stream;
            stream << "new request \n{\n" << indent << "method : \"" << request.method << "\" \n";
            stream << indent << "url : \"" << request.url << "\"\n";
            stream << indent << "http version : \"" << request.http_version << "\"\n";
            stream << indent << "header fields : \n" << indent << "{\n";
            for (auto &[name, value] : request.header_fields)
            {
                stream << indent << indent << "\"" << name << "\" : \"" << value << "\"\n";
            }
            stream << indent << "}end header fields\n";
            stream << "}end request\n";
            auto lock = logger.lock();
            logger << stream.str() << logger.flush;
        }
        static void log_exception(logger_t & logger, http_exception const&e)
        {
            std::ostringstream stream;
            stream << "exception caught : http status code = " << e.error_code << "\n";
            stream << "what() : \"" << e.what() << "\"\n";
            auto lock = logger.lock();
            logger << stream.str() << logger.flush;
        }
        void log_socket(logger_t &logger)
        {
            std::ostringstream stream;
            stream << "process connection from ip:\"" << socket.get_address()
                   << "\" and port:\"" << std::to_string(socket.get_port()) << "\".\n";
            auto lock = logger.lock();
            logger << stream.str() << logger.flush;
        }
        static std::string_view to_string_view(std::csub_match match)
        {
            return {match.first, static_cast<std::string_view::size_type>(match.second - match.first)};
        }

        inline static constexpr std::string_view indent = "    ";
        using handler_type = std::function<void(http_connection &, http_request const &)>;
        using method_map_type = std::unordered_map<std::string_view, handler_type>;
        inline static method_map_type method_map =
            {
                {"GET", &http_connection::method_get}
            };

    };
}
#endif //NETWORK_EXPR_HTTP_CONNECTION_HPP
