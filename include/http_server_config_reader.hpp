#pragma once
#include <string_view>                           // for std::string_view
#include <lzhlib/stdlib_ext/type_traits_ext.hpp> // for lzhlib::remove_cvrf_t
#include "config_file_reader.hpp"
#include "http_server_config.hpp"
namespace lzhlib
{

    class http_server_config_reader
    {
    private:
        using type_list = reader::type_list<reader::type<remove_cvrf_t<decltype(http_server_config::max_connections)>>,
                                            reader::type<remove_cvrf_t<decltype(http_server_config::buffer_length)>>,
                                            reader::type<remove_cvrf_t<decltype(http_server_config::server_address)>>,
                                            reader::type<remove_cvrf_t<decltype(http_server_config::server_port)>>,
                                            reader::type<remove_cvrf_t<decltype(http_server_config::document_root)>>>;
        static constexpr std::size_t var_num = type_list::size;
        inline static constexpr std::array<std::string_view, var_num> variables =
            {"max_connections",
             "buffer_length",
             "server_address",
             "server_port",
             "document_root"
            };
        config_file_reader<type_list> reader;
    public:
        explicit http_server_config_reader(std::string_view path = http_server_config_reader::config_file_path)
            : reader(variables)
        {
        }
        http_server_config read(std::string_view path = http_server_config_reader::config_file_path)
        {
            reader.read(path);
            return read_helper(std::make_index_sequence<var_num>());
        }
    private:
        template <std::size_t ...Indices>
        http_server_config read_helper(std::index_sequence<Indices...>)
        {
            http_server_config config{reader.get<Indices>()...};
            namespace fs = std::filesystem;
            fs::path config_file(config_file_path);
            config.document_root = fs::canonical(config_file.parent_path() / config.document_root).string();
            return config;
        }
        inline static constexpr std::string_view config_file_path = "./srv.conf";
    };
}
