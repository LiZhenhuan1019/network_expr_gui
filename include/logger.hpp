#ifndef NETWORK_EXPR_LOGGER_HPP
#define NETWORK_EXPR_LOGGER_HPP
#include <ostream>     // for std::ostream
#include <sstream>     // for std::ostringstream
#include <type_traits> // for std::enable_if_t, std::is_invocable_v
#include <utility>     // for std::forward
#include <string>      // for std::string
#include <functional>  // for std::function
#include <mutex>       // for std::mutex, std::lock_guard
namespace lzhlib
{

    struct log_flush_t
    {
        explicit log_flush_t() = default;
    };
    inline constexpr log_flush_t flush;

    template <typename StreamT = std::ostream>
    class stream_logger
    {
        using stream_t = StreamT;
    public:
        explicit stream_logger(stream_t &ostream)
            : stream(ostream)
        {}
        template <typename ...Args>
        stream_logger &log(Args &&...args)
        {
            return log_impl(std::forward<Args>(args)...);

        }
        template <typename T>
        stream_logger &operator<<(T &&t)
        {
            return log_impl(std::forward<T>(t));
        }

        static constexpr log_flush_t flush{};
    private:
        stream_t &stream;

        template <typename ...Args>
        stream_logger &log_impl(Args &&...args)
        {
            (stream << ... << std::forward<Args>(args));
            return *this;
        }
        template <typename T, std::enable_if_t<std::is_invocable_v<T &&, stream_logger &>>>
        stream_logger &log_impl(T &&t)
        {
            std::forward<T>(t)(*this);
            return *this;
        }
    };
    class string_logger
    {
    public:
        template <typename ...Args>
        string_logger &log(Args &&...args)
        {
            return log_impl(std::forward<Args>(args)...);
        }
        template <typename T>
        string_logger &operator<<(T &&t)
        {
            return log_impl(std::forward<T>(t));
        }
        std::string str() const
        {
            return stream.str();
        }
        void clear()
        {
            stream.str("");
        }
        template <typename Callable, typename = std::enable_if_t<std::is_invocable_v<Callable, string_logger &>>>
        void set_flush_handler(Callable callable)
        {
            flush_handler = callable;
        }

        static constexpr log_flush_t flush{};
    private:
        std::function<void(string_logger &)> flush_handler;
        std::ostringstream stream;
        stream_logger<std::ostream> logger{stream};

        template <typename ...Args>
        string_logger &log_impl(Args &&...args)
        {
            logger.log(std::forward<Args>(args)...);
            return *this;
        }
        template <typename T, typename = std::enable_if_t<std::is_invocable_v<T &&, string_logger &>>>
        string_logger &log_impl(T &&t)
        {
            std::forward<T>(t)(*this);
            return *this;
        }
        string_logger &log_impl(log_flush_t)
        {
            if (flush_handler)
                flush_handler(*this);
            return *this;
        }
    };
    template <typename LoggerT>
    struct thread_safe_logger
    {
        using logger_t = LoggerT;
        explicit thread_safe_logger(logger_t &logger)
            : logger(logger)
        {}
        template <typename ...Args>
        thread_safe_logger &safe_log(Args &&...args)
        {
            auto lock_guard = lock();
            logger.log(std::forward<Args>(args)...);
            return *this;
        }
        template <typename ...Args>
        thread_safe_logger &log(Args &&...args)
        {
            logger.log(std::forward<Args>(args)...);
            return *this;
        }
        template <typename T>
        thread_safe_logger &operator<<(T &&t)
        {
            logger << std::forward<T>(t);
            return *this;
        }
        std::lock_guard<std::mutex> lock()
        {
            return std::lock_guard<std::mutex>(logger_mutex);
        }
        static constexpr log_flush_t flush{};
    private:
        std::mutex logger_mutex;
        logger_t &logger;
    };
}
#endif //NETWORK_EXPR_LOGGER_HPP
