#ifndef NETWORK_EXPR_HTTP_SERVER_CONFIG_HPP
#define NETWORK_EXPR_HTTP_SERVER_CONFIG_HPP
#include <cstddef> // for std::size_t
#include <string>  // for std::string
namespace lzhlib
{
    struct http_server_config
    {
        std::size_t max_connections;
        std::size_t buffer_length;
        std::string server_address;
        std::uint16_t server_port;
        std::string document_root;
    };
}
#endif //NETWORK_EXPR_HTTP_SERVER_CONFIG_HPP
