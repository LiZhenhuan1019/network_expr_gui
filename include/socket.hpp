#ifndef NETWORK_EXPR_SOCKET_HPP
#define NETWORK_EXPR_SOCKET_HPP
#include <stdexcept>   // for std::runtime_error
#include <string>      // for std::string
#include <utility>     // for std::exchange, std::swap, std::pair, std::move
#include <algorithm>   // for std::min
#include <cstdint>     // for std::uint16_t
#include <cstddef>     // for std::size_t
#include <string_view> // std::string_view
#include <winsock2.h>
#include "http_server_config.hpp"
namespace lzhlib
{
    struct socket_error : std::runtime_error
    {
        explicit socket_error(std::string const &s)
            : runtime_error(s)
        {}
    };
    struct socket_create_error : socket_error
    {
        explicit socket_create_error(std::string const &s)
            : socket_error(s)
        {}
    };
    struct socket_accept_error : socket_error
    {
        explicit socket_accept_error(std::string const &s)
            : socket_error(s)
        {}
    };
    struct socket_send_error : socket_error
    {
        explicit socket_send_error(std::string const &s)
            : socket_error(s)
        {}
    };
    struct socket_receive_error : socket_error
    {
        explicit socket_receive_error(std::string const &s)
            : socket_error(s)
        {}
    };
    struct socket_view
    {
        explicit socket_view(SOCKET s)
            : socket(s)
        {}
        SOCKET socket;
    };
    struct socket_handler
    {
        explicit socket_handler(SOCKET s)
            : socket(s)
        {}
        socket_handler(socket_handler const &) = delete;
        socket_handler(socket_handler &&src) noexcept
            : socket(std::exchange(src.socket, INVALID_SOCKET))
        {}
        socket_handler &operator=(socket_handler const &src) = delete;
        socket_handler &operator=(socket_handler &&src) noexcept
        {
            std::swap(socket, src.socket);
            return *this;
        }
        socket_view view() const noexcept
        {
            return socket_view(socket);
        }
        explicit operator socket_view() const noexcept
        {
            return view();
        }
        ~socket_handler()
        {
            if (socket != INVALID_SOCKET)
                closesocket(socket);
        }
        SOCKET socket;
    };
    class tcp_communicate_socket
    {
    public:
        explicit tcp_communicate_socket(socket_view socket, sockaddr_in const &client_addr, std::size_t buffer_length)
            : socket(socket), addr(client_addr), buffer_length(buffer_length)
        {}
        void send(std::string_view str)
        {
            auto size = str.size();
            while (size > 0)
            {
                auto sent_size = ::send(socket.socket, str.data(), static_cast<int>(std::min(size, buffer_length)), 0);
                if (sent_size < 0)
                    throw socket_send_error("::send failed.");
                str.remove_prefix(std::size_t(sent_size));
                if (size > sent_size)
                    size -= sent_size;
                else
                    break;
            }
        }
        int receive(std::string &data)
        {
            data.resize(buffer_length);
            int result = recv(socket.socket, data.data(), static_cast<int>(buffer_length), MSG_PEEK);
            if (result < 0)
                throw socket_receive_error("::recv failed.");
            data.resize(static_cast<std::size_t>(result));
            return result;
        }
        std::string get_address() const
        {
            return inet_ntoa(addr.sin_addr);
        }
        std::uint16_t get_port() const
        {
            return ntohs(addr.sin_port);
        }
    private:
        socket_view socket;
        sockaddr_in addr;
        std::size_t buffer_length;
    };
    class tcp_listen_socket
    {
    public:
        explicit tcp_listen_socket(socket_view socket, std::size_t buffer_length)
            : socket(socket), buffer_length(buffer_length)
        {
        }
        std::pair<socket_handler, tcp_communicate_socket> accept()
        {
            sockaddr_in client_address{};
            int length = sizeof(client_address);
            socket_handler new_socket{::accept(socket.socket, to_sockaddr(&client_address), &length)};
            if (new_socket.socket == INVALID_SOCKET)
                throw socket_accept_error("::accept returns INVALID_SOCKET.");
            if (length != sizeof(client_address))
                throw socket_accept_error("address that accepted is not compatible. ");
            auto view = new_socket.view();
            return {std::move(new_socket), tcp_communicate_socket{view, client_address, buffer_length}};
        }
        socket_view view() const noexcept
        {
            return socket;
        }
        static std::pair<socket_handler, tcp_listen_socket> create(http_server_config const &config)
        {
            socket_handler handler(::socket(AF_INET, SOCK_STREAM, 0));
            if (handler.socket == INVALID_SOCKET)
                throw socket_create_error("create socket failed.");
            sockaddr_in addr{};
            addr.sin_family = AF_INET;
            addr.sin_addr.S_un.S_addr = inet_addr(config.server_address.data());
            addr.sin_port = htons(config.server_port);
            if (::bind(handler.socket, to_sockaddr(&addr), sizeof(addr)) == SOCKET_ERROR)
                throw socket_create_error("socket bind failed.");
            if (::listen(handler.socket, config.max_connections) == SOCKET_ERROR)
                throw socket_create_error("socket listen failed.");
            return {std::move(handler), tcp_listen_socket{handler.view(), config.buffer_length}};
        }
    private:
        socket_view socket;
        std::size_t buffer_length;
        static SOCKADDR *to_sockaddr(SOCKADDR_IN *p)
        {
            return reinterpret_cast<SOCKADDR *>(p);
        }
    };
}
#endif //NETWORK_EXPR_SOCKET_HPP
