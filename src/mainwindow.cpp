#include "include/mainwindow.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    logger(string_logger)
{
    ui->setupUi(this);
    connect(this, SIGNAL(change_text()), this, SLOT(changing_text()));
    string_logger.set_flush_handler([this](lzhlib::string_logger&logger)
    {
        logger_outputs.push(logger.str());
        emit change_text();
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::start()
{
    config.emplace();
    server.emplace(config->read(), logger);
}

void MainWindow::stop()
{
    server.reset();
    config.reset();
    update_output("");
}

void MainWindow::changing_text()
{
    using namespace std::chrono_literals;
    if (auto str = logger_outputs.wait_for_and_pop(50ms))
    {
        update_output(str.value());
    }
}

void MainWindow::on_start_button_clicked()
{
    start();
}

void MainWindow::on_stop_button_clicked()
{
    stop();
}

void MainWindow::update_output(std::string_view s)
{
    ui->logger_output->setPlainText(s.data());
}
